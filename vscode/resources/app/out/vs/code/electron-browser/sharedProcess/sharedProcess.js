/*---------------------------------------------------------------------------------------------
 *  Copyright (c) Microsoft Corporation. All rights reserved.
 *  Licensed under the MIT License. See License.txt in the project root for license information.
 *--------------------------------------------------------------------------------------------*/
"use strict";const bootstrap=require("../../../../bootstrap"),bootstrapWindow=require("../../../../bootstrap-window");bootstrap.avoidMonkeyPatchFromAppInsights(),bootstrapWindow.load(["vs/code/electron-browser/sharedProcess/sharedProcessMain"],function(o,t){o.startup({machineId:t.machineId})});
//# sourceMappingURL=https://ticino.blob.core.windows.net/sourcemaps/dea8705087adb1b5e5ae1d9123278e178656186a/core/vs\code\electron-browser\sharedProcess\sharedProcess.js.map
