var searchData=
[
  ['received',['received',['../classwpi_1_1uv_1_1Udp.html#a19b611c5c67e4699ef57105191c68d7d',1,'wpi::uv::Udp']]],
  ['remote_5fid',['remote_id',['../structNT__ConnectionInfo.html#abc07959851c53c5d378fbd8ac6797bb8',1,'NT_ConnectionInfo::remote_id()'],['../structnt_1_1ConnectionInfo.html#abd6756b834c10c0b0ddbef85003d4efc',1,'nt::ConnectionInfo::remote_id()']]],
  ['remote_5fip',['remote_ip',['../structNT__ConnectionInfo.html#a30da15ce52b23bbbc867a201548e1d48',1,'NT_ConnectionInfo::remote_ip()'],['../structnt_1_1ConnectionInfo.html#ac68ecd3cbafe77f22de289bd2b0f79ce',1,'nt::ConnectionInfo::remote_ip()']]],
  ['remote_5fport',['remote_port',['../structNT__ConnectionInfo.html#a62e6f419f8918e0e3c72aa2a627685e1',1,'NT_ConnectionInfo::remote_port()'],['../structnt_1_1ConnectionInfo.html#a374d9048e0a8f8b877ec15ce4f20ec02',1,'nt::ConnectionInfo::remote_port()']]],
  ['resolved',['resolved',['../classwpi_1_1uv_1_1GetAddrInfoReq.html#a64960214931e8f3297f01fbe2190fe07',1,'wpi::uv::GetAddrInfoReq::resolved()'],['../classwpi_1_1uv_1_1GetNameInfoReq.html#a52436cd4e22f1a492c335b675ac1d621',1,'wpi::uv::GetNameInfoReq::resolved()']]]
];
