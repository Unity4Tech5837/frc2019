'use strict';
Object.defineProperty(exports, "__esModule", { value: true });
const fs = require("fs");
const jsonc = require("jsonc-parser");
const path = require("path");
const logger_1 = require("../logger");
const vscode = require("../vscodeshim");
const generator_1 = require("./generator");
class Examples {
    constructor(resourceRoot, java, core) {
        this.exampleResourceName = 'examples.json';
        const examplesFolder = path.join(resourceRoot, 'src', 'examples');
        const resourceFile = path.join(examplesFolder, this.exampleResourceName);
        const gradleBasePath = path.join(path.dirname(resourceRoot), 'gradle');
        fs.readFile(resourceFile, 'utf8', (err, data) => {
            if (err) {
                logger_1.logger.log('Example Error error: ', err);
                return;
            }
            const examples = jsonc.parse(data);
            for (const e of examples) {
                const provider = {
                    getLanguage() {
                        return java ? 'java' : 'cpp';
                    },
                    getDescription() {
                        return e.description;
                    },
                    getDisplayName() {
                        return e.name;
                    },
                    async generate(folderInto) {
                        try {
                            if (java) {
                                if (!await generator_1.generateCopyJava(path.join(examplesFolder, e.foldername), path.join(gradleBasePath, e.gradlebase), folderInto.fsPath, 'frc.robot.Main', path.join('frc', 'robot'))) {
                                    vscode.window.showErrorMessage('Cannot create into non empty folder');
                                    return false;
                                }
                            }
                            else {
                                if (!await generator_1.generateCopyCpp(path.join(examplesFolder, e.foldername), path.join(gradleBasePath, e.gradlebase), folderInto.fsPath, false)) {
                                    vscode.window.showErrorMessage('Cannot create into non empty folder');
                                    return false;
                                }
                            }
                        }
                        catch (err) {
                            logger_1.logger.error('Example generation error: ', err);
                            return false;
                        }
                        return true;
                    },
                };
                core.addExampleProvider(provider);
            }
        });
    }
    // tslint:disable-next-line:no-empty
    dispose() {
    }
}
exports.Examples = Examples;
//# sourceMappingURL=examples.js.map