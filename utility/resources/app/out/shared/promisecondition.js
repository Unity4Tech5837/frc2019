'use strict';
Object.defineProperty(exports, "__esModule", { value: true });
class PromiseCondition {
    constructor(defaultValue) {
        this.hasBeenSet = false;
        this.condSet = undefined;
        this.value = defaultValue;
    }
    wait() {
        return new Promise((resolve, _) => {
            this.condSet = (value) => {
                resolve(value);
            };
            if (this.hasBeenSet === true) {
                resolve(this.value);
            }
        });
    }
    set(value) {
        this.value = value;
        this.hasBeenSet = true;
        if (this.condSet !== undefined) {
            this.condSet(value);
        }
    }
    reset(value) {
        this.value = value;
        this.condSet = undefined;
        this.hasBeenSet = false;
    }
}
exports.PromiseCondition = PromiseCondition;
//# sourceMappingURL=promisecondition.js.map