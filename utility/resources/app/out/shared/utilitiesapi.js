'use strict';
Object.defineProperty(exports, "__esModule", { value: true });
const os = require("os");
const path = require("path");
const utilities_1 = require("../utilities");
class UtilitiesAPI {
    getFrcYear() {
        return '2019';
    }
    getWPILibHomeDir() {
        if (this.wpilibHome) {
            return this.wpilibHome;
        }
        const year = this.getFrcYear();
        if (utilities_1.getIsWindows()) {
            let publicFolder = process.env.PUBLIC;
            if (!publicFolder) {
                publicFolder = 'C:\\Users\\Public';
            }
            this.wpilibHome = path.join(publicFolder, `frc${year}`);
        }
        else {
            const dir = os.homedir();
            this.wpilibHome = path.join(dir, `frc${year}`);
        }
        return this.wpilibHome;
    }
}
exports.UtilitiesAPI = UtilitiesAPI;
//# sourceMappingURL=utilitiesapi.js.map