'use strict';
Object.defineProperty(exports, "__esModule", { value: true });
const fetch = require("node-fetch");
const path = require("path");
const logger_1 = require("../logger");
const generator_1 = require("../shared/generator");
const utilities_1 = require("../utilities");
// tslint:disable-next-line:no-any
function isJsonDependency(arg) {
    const jsonDep = arg;
    return jsonDep.jsonUrl !== undefined && jsonDep.name !== undefined
        && jsonDep.uuid !== undefined && jsonDep.version !== undefined;
}
exports.isJsonDependency = isJsonDependency;
class VendorLibrariesBase {
    constructor(utilities) {
        this.utilities = utilities;
    }
    async findForUUIDs(uuid) {
        const homeDirDeps = await this.getHomeDirDeps();
        const foundDeps = homeDirDeps.filter((value) => {
            return uuid.indexOf(value.uuid) >= 0;
        });
        return foundDeps;
    }
    getVendorFolder(root) {
        return path.join(root, 'vendordeps');
    }
    async installDependency(dep, url, override) {
        const dirExists = await utilities_1.promisifyExists(url);
        if (!dirExists) {
            await generator_1.promisifyMkdirp(url);
            // Directly write file
            await utilities_1.promisifyWriteFile(path.join(url, dep.fileName), JSON.stringify(dep, null, 4));
            return true;
        }
        const files = await generator_1.promisifyReadDir(url);
        for (const file of files) {
            const fullPath = path.join(url, file);
            const result = await this.readFile(fullPath);
            if (result !== undefined) {
                if (result.uuid === dep.uuid) {
                    if (override) {
                        await utilities_1.promisifyDeleteFile(fullPath);
                        break;
                    }
                    else {
                        return false;
                    }
                }
            }
        }
        await utilities_1.promisifyWriteFile(path.join(url, dep.fileName), JSON.stringify(dep, null, 4));
        return true;
    }
    getHomeDirDeps() {
        return this.getDependencies(path.join(this.utilities.getWPILibHomeDir(), 'vendordeps'));
    }
    async readFile(file) {
        try {
            const jsonContents = await utilities_1.promisifyReadFile(file);
            const dep = JSON.parse(jsonContents);
            if (isJsonDependency(dep)) {
                return dep;
            }
            return undefined;
        }
        catch (err) {
            logger_1.logger.warn('JSON parse error', err);
            return undefined;
        }
    }
    async getDependencies(dir) {
        try {
            const files = await generator_1.promisifyReadDir(dir);
            const promises = [];
            for (const file of files) {
                promises.push(this.readFile(path.join(dir, file)));
            }
            const results = await Promise.all(promises);
            return results.filter((x) => x !== undefined);
        }
        catch (err) {
            return [];
        }
    }
    async loadFileFromUrl(url) {
        try {
            const response = await fetch.default(url, {
                timeout: 5000,
            });
            if (response === undefined) {
                return undefined;
            }
            if (response.status >= 200 && response.status <= 300) {
                try {
                    const text = await response.text();
                    const json = JSON.parse(text);
                    if (isJsonDependency(json)) {
                        return json;
                    }
                    else {
                        return undefined;
                    }
                }
                catch (_a) {
                    return undefined;
                }
            }
            else {
                return undefined;
            }
        }
        catch (err) {
            logger_1.logger.log('Error fetching file', err);
            return undefined;
        }
    }
}
exports.VendorLibrariesBase = VendorLibrariesBase;
//# sourceMappingURL=vendorlibrariesbase.js.map