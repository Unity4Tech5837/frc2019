"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const fs = require("fs");
const glob = require("glob");
const mkdirp = require("mkdirp");
const ncp = require("ncp");
const path = require("path");
const logger_1 = require("../logger");
const utilities_1 = require("../utilities");
const permissions_1 = require("./permissions");
function promisifyMkdirp(dest) {
    return new Promise((resolve, reject) => {
        mkdirp(dest, (err) => {
            if (err) {
                reject(err);
            }
            else {
                resolve();
            }
        });
    });
}
exports.promisifyMkdirp = promisifyMkdirp;
function promisifyNcp(source, dest, options = {}) {
    return promisifyMkdirp(dest).then(() => {
        return new Promise((resolve, reject) => {
            ncp.ncp(source, dest, options, (err) => {
                if (err) {
                    reject(err);
                }
                resolve();
            });
        });
    });
}
exports.promisifyNcp = promisifyNcp;
function promisifyReadDir(pth) {
    return new Promise((resolve, reject) => {
        fs.readdir(pth, (err, files) => {
            if (err) {
                reject(err);
            }
            else {
                resolve(files);
            }
        });
    });
}
exports.promisifyReadDir = promisifyReadDir;
async function generateCopyCpp(fromTemplateFolder, fromGradleFolder, toFolder, addCpp) {
    const existingFiles = await promisifyReadDir(toFolder);
    if (existingFiles.length > 0) {
        logger_1.logger.warn('folder not empty');
        return false;
    }
    let codePath = path.join(toFolder, 'src', 'main');
    if (addCpp) {
        codePath = path.join(codePath, 'cpp');
    }
    const gradleRioFrom = '###GRADLERIOREPLACE###';
    const grRoot = path.dirname(fromGradleFolder);
    const grVersionFile = path.join(grRoot, 'version.txt');
    const grVersionTo = (await utilities_1.promisifyReadFile(grVersionFile)).trim();
    if (typeof fromTemplateFolder === 'string') {
        await promisifyNcp(fromTemplateFolder, codePath);
    }
    else {
        await fromTemplateFolder(codePath, toFolder);
    }
    await promisifyNcp(fromGradleFolder, toFolder, {
        filter: (cf) => {
            const rooted = path.relative(fromGradleFolder, cf);
            if (rooted.startsWith('bin') || rooted.indexOf('.project') >= 0) {
                return false;
            }
            return true;
        },
    });
    await promisifyNcp(path.join(grRoot, 'shared'), toFolder, {
        filter: (cf) => {
            const rooted = path.relative(fromGradleFolder, cf);
            if (rooted.startsWith('bin') || rooted.indexOf('.project') >= 0) {
                return false;
            }
            return true;
        },
    });
    await permissions_1.setExecutePermissions(path.join(toFolder, 'gradlew'));
    const buildgradle = path.join(toFolder, 'build.gradle');
    await new Promise((resolve, reject) => {
        fs.readFile(buildgradle, 'utf8', (err, dataIn) => {
            if (err) {
                resolve();
            }
            else {
                const dataOut = dataIn.replace(new RegExp(gradleRioFrom, 'g'), grVersionTo);
                fs.writeFile(buildgradle, dataOut, 'utf8', (err1) => {
                    if (err1) {
                        reject(err);
                    }
                    else {
                        resolve();
                    }
                });
            }
        });
    });
    const deployDir = path.join(toFolder, 'src', 'main', 'deploy');
    await promisifyMkdirp(deployDir);
    await utilities_1.promisifyWriteFile(path.join(deployDir, 'example.txt'), `Files placed in this directory will be deployed to the RoboRIO into the
'deploy' directory in the home folder. Use the 'frc::filesystem::GetDeployDirectory'
function from the 'frc/Filesystem.h' header to get a proper path relative to the deploy
directory.`);
    return true;
}
exports.generateCopyCpp = generateCopyCpp;
async function generateCopyJava(fromTemplateFolder, fromGradleFolder, toFolder, robotClassTo, copyRoot, packageReplaceString) {
    const existingFiles = await promisifyReadDir(toFolder);
    if (existingFiles.length > 0) {
        return false;
    }
    const rootCodePath = path.join(toFolder, 'src', 'main', 'java');
    const codePath = path.join(rootCodePath, copyRoot);
    if (typeof fromTemplateFolder === 'string') {
        await promisifyNcp(fromTemplateFolder, codePath);
    }
    else {
        await fromTemplateFolder(codePath, toFolder);
    }
    const files = await new Promise((resolve, reject) => {
        glob('**/*', {
            cwd: codePath,
            nodir: true,
            nomount: true,
        }, (err, matches) => {
            if (err) {
                reject(err);
            }
            else {
                resolve(matches);
            }
        });
    });
    // Package replace inside the template
    const replacePackageFrom = 'edu\\.wpi\\.first\\.wpilibj\\.(?:examples|templates)\\..+?(?=;|\\.)';
    const replacePackageTo = 'frc.robot';
    const robotClassFrom = '###ROBOTCLASSREPLACE###';
    const gradleRioFrom = '###GRADLERIOREPLACE###';
    const grRoot = path.dirname(fromGradleFolder);
    const grVersionFile = path.join(grRoot, 'version.txt');
    const grVersionTo = (await utilities_1.promisifyReadFile(grVersionFile)).trim();
    const promiseArray = [];
    for (const f of files) {
        const file = path.join(codePath, f);
        promiseArray.push(new Promise((resolve, reject) => {
            fs.readFile(file, 'utf8', (err, dataIn) => {
                if (err) {
                    reject(err);
                }
                else {
                    let dataOut = dataIn.replace(new RegExp(replacePackageFrom, 'g'), replacePackageTo);
                    if (packageReplaceString !== undefined) {
                        dataOut = dataOut.replace(new RegExp(packageReplaceString, 'g'), replacePackageTo);
                    }
                    fs.writeFile(file, dataOut, 'utf8', (err1) => {
                        if (err1) {
                            reject(err);
                        }
                        else {
                            resolve();
                        }
                    });
                }
            });
        }));
    }
    await promisifyNcp(fromGradleFolder, toFolder, {
        filter: (cf) => {
            const rooted = path.relative(fromGradleFolder, cf);
            if (rooted.startsWith('bin') || rooted.indexOf('.project') >= 0) {
                return false;
            }
            return true;
        },
    });
    await promisifyNcp(path.join(grRoot, 'shared'), toFolder, {
        filter: (cf) => {
            const rooted = path.relative(fromGradleFolder, cf);
            if (rooted.startsWith('bin') || rooted.indexOf('.project') >= 0) {
                return false;
            }
            return true;
        },
    });
    await permissions_1.setExecutePermissions(path.join(toFolder, 'gradlew'));
    const buildgradle = path.join(toFolder, 'build.gradle');
    await new Promise((resolve, reject) => {
        fs.readFile(buildgradle, 'utf8', (err, dataIn) => {
            if (err) {
                resolve();
            }
            else {
                const dataOut = dataIn.replace(new RegExp(robotClassFrom, 'g'), robotClassTo)
                    .replace(new RegExp(gradleRioFrom, 'g'), grVersionTo);
                fs.writeFile(buildgradle, dataOut, 'utf8', (err1) => {
                    if (err1) {
                        reject(err);
                    }
                    else {
                        resolve();
                    }
                });
            }
        });
    });
    const deployDir = path.join(toFolder, 'src', 'main', 'deploy');
    await promisifyMkdirp(deployDir);
    await utilities_1.promisifyWriteFile(path.join(deployDir, 'example.txt'), `Files placed in this directory will be deployed to the RoboRIO into the
'deploy' directory in the home folder. Use the 'Filesystem.getDeployDirectory' wpilib function
to get a proper path relative to the deploy directory.`);
    return true;
}
exports.generateCopyJava = generateCopyJava;
function setDesktopEnabled(buildgradle, setting) {
    return new Promise((resolve, reject) => {
        fs.readFile(buildgradle, 'utf8', (err, dataIn) => {
            if (err) {
                resolve();
            }
            else {
                const dataOut = dataIn.replace(/def\s+includeDesktopSupport\s*=\s*(true|false)/gm, `def includeDesktopSupport = ${setting ? 'true' : 'false'}`);
                fs.writeFile(buildgradle, dataOut, 'utf8', (err1) => {
                    if (err1) {
                        reject(err);
                    }
                    else {
                        resolve();
                    }
                });
            }
        });
    });
}
exports.setDesktopEnabled = setDesktopEnabled;
//# sourceMappingURL=generator.js.map