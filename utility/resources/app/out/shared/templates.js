'use strict';
Object.defineProperty(exports, "__esModule", { value: true });
const fs = require("fs");
const jsonc = require("jsonc-parser");
const path = require("path");
const logger_1 = require("../logger");
const vscode = require("../vscodeshim");
const generator_1 = require("./generator");
class Templates {
    constructor(resourceRoot, java, core) {
        this.exampleResourceName = 'templates.json';
        const templatesFolder = path.join(resourceRoot, 'src', 'templates');
        const resourceFile = path.join(templatesFolder, this.exampleResourceName);
        const gradleBasePath = path.join(path.dirname(resourceRoot), 'gradle');
        fs.readFile(resourceFile, 'utf8', (err, data) => {
            if (err) {
                logger_1.logger.log('Template error: ', err);
                return;
            }
            const templates = jsonc.parse(data);
            for (const e of templates) {
                const provider = {
                    getLanguage() {
                        return java ? 'java' : 'cpp';
                    },
                    getDescription() {
                        return e.description;
                    },
                    getDisplayName() {
                        return e.name;
                    },
                    async generate(folderInto) {
                        try {
                            if (java) {
                                if (!await generator_1.generateCopyJava(path.join(templatesFolder, e.foldername), path.join(gradleBasePath, e.gradlebase), folderInto.fsPath, 'frc.robot.Main', path.join('frc', 'robot'))) {
                                    vscode.window.showErrorMessage('Cannot create into non empty folder');
                                    return false;
                                }
                            }
                            else {
                                if (!await generator_1.generateCopyCpp(path.join(templatesFolder, e.foldername), path.join(gradleBasePath, e.gradlebase), folderInto.fsPath, false)) {
                                    vscode.window.showErrorMessage('Cannot create into non empty folder');
                                    return false;
                                }
                            }
                        }
                        catch (err) {
                            logger_1.logger.error('template creation error', err);
                            return false;
                        }
                        return true;
                    },
                };
                core.addTemplateProvider(provider);
            }
        });
    }
    // tslint:disable-next-line:no-empty
    dispose() {
    }
}
exports.Templates = Templates;
//# sourceMappingURL=templates.js.map