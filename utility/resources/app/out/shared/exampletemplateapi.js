"use strict";
'use scrict';
Object.defineProperty(exports, "__esModule", { value: true });
const path = require("path");
const utilities_1 = require("../utilities");
const vscode = require("../vscodeshim");
const generator_1 = require("./generator");
class ExampleTemplateAPI {
    constructor() {
        this.templates = [];
        this.examples = [];
    }
    addTemplateProvider(provider) {
        const lp = {
            creator: provider,
            description: provider.getDescription(),
            label: provider.getDisplayName(),
        };
        this.templates.push(lp);
    }
    addExampleProvider(provider) {
        const lp = {
            creator: provider,
            description: provider.getDescription(),
            label: provider.getDisplayName(),
        };
        this.examples.push(lp);
    }
    getLanguages(template) {
        const retSet = new Set();
        if (template) {
            for (const t of this.templates) {
                retSet.add(t.creator.getLanguage());
            }
        }
        else {
            for (const t of this.examples) {
                retSet.add(t.creator.getLanguage());
            }
        }
        return Array.from(retSet);
    }
    getBases(template, language) {
        const ret = [];
        if (template) {
            for (const t of this.templates) {
                if (t.creator.getLanguage() === language) {
                    ret.push(t);
                }
            }
        }
        else {
            for (const t of this.examples) {
                if (t.creator.getLanguage() === language) {
                    ret.push(t);
                }
            }
        }
        return ret;
    }
    dispose() {
        //
    }
    async createProject(template, language, base, toFolder, newFolder, projectName, teamNumber) {
        if (template) {
            for (const t of this.templates) {
                if (t.creator.getLanguage() === language && t.label === base) {
                    return this.handleGenerate(t.creator, toFolder, newFolder, projectName, teamNumber);
                }
            }
        }
        else {
            for (const t of this.examples) {
                if (t.creator.getLanguage() === language && t.label === base) {
                    return this.handleGenerate(t.creator, toFolder, newFolder, projectName, teamNumber);
                }
            }
        }
        return false;
    }
    async handleGenerate(creator, toFolderOrig, newFolder, projectName, teamNumber) {
        let toFolder = toFolderOrig;
        if (newFolder) {
            toFolder = path.join(toFolderOrig, projectName);
        }
        try {
            await generator_1.promisifyMkdirp(toFolder);
        }
        catch (_a) {
            //
        }
        const toFolderUri = vscode.Uri.file(toFolder);
        const success = await creator.generate(toFolderUri);
        if (!success) {
            return false;
        }
        const jsonFilePath = path.join(toFolder, '.wpilib', 'wpilib_preferences.json');
        const parsed = JSON.parse(await utilities_1.promisifyReadFile(jsonFilePath));
        parsed.teamNumber = teamNumber;
        await utilities_1.promisifyWriteFile(jsonFilePath, JSON.stringify(parsed, null, 4));
        return true;
    }
}
exports.ExampleTemplateAPI = ExampleTemplateAPI;
//# sourceMappingURL=exampletemplateapi.js.map