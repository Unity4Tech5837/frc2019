"use strict";
'use scrict';
Object.defineProperty(exports, "__esModule", { value: true });
const jsonc = require("jsonc-parser");
const path = require("path");
const logger_1 = require("../logger");
const utilities_1 = require("../utilities");
const vscode = require("../vscodeshim");
const generator_1 = require("./generator");
// tslint:disable-next-line:no-any
function isJsonExample(arg) {
    const jsonDep = arg;
    return jsonDep.name !== undefined && jsonDep.description !== undefined
        && jsonDep.gradlebase !== undefined && jsonDep.dependencies !== undefined
        && jsonDep.files !== undefined && jsonDep.language !== undefined;
}
async function addVendorExamples(resourceRoot, core, utilities, vendorlibs) {
    const storagePath = utilities_1.extensionContext.storagePath;
    if (storagePath === undefined) {
        return;
    }
    const exampleDir = path.join(utilities.getWPILibHomeDir(), 'vendorexamples');
    const gradleBasePath = path.join(resourceRoot, 'gradle');
    if (await utilities_1.promisifyExists(exampleDir)) {
        const files = await generator_1.promisifyReadDir(exampleDir);
        for (const file of files) {
            const fileContents = await utilities_1.promisifyReadFile(path.join(exampleDir, file));
            const parsed = jsonc.parse(fileContents);
            if (Array.isArray(parsed)) {
                for (const ex of parsed) {
                    if (isJsonExample(ex)) {
                        if (ex.language !== 'java' && ex.language !== 'cpp') {
                            // Only handle java and cpp
                            continue;
                        }
                        const provider = {
                            getLanguage() {
                                return ex.language;
                            },
                            getDescription() {
                                return ex.description;
                            },
                            getDisplayName() {
                                return ex.name;
                            },
                            async generate(folderInto) {
                                try {
                                    if (ex.language === 'java') {
                                        if (!await generator_1.generateCopyJava(async (copyPath, rootDir) => {
                                            for (const copyFile of ex.files) {
                                                const copyFilePath = path.join(copyPath, copyFile.deployloc);
                                                const copyParent = path.dirname(copyFilePath);
                                                await generator_1.promisifyMkdirp(copyParent);
                                                await utilities_1.promisifyWriteFile(copyFilePath, copyFile.contents);
                                            }
                                            const vendorFiles = await vendorlibs.findForUUIDs(ex.dependencies);
                                            for (const vendorFile of vendorFiles) {
                                                await vendorlibs.installDependency(vendorFile, vendorlibs.getVendorFolder(rootDir), true);
                                            }
                                            return true;
                                        }, path.join(gradleBasePath, ex.gradlebase), folderInto.fsPath, 'frc.robot.' + ex.mainclass, path.join('frc', 'robot'), ex.packagetoreplace)) {
                                            vscode.window.showErrorMessage('Cannot create into non empty folder');
                                            return false;
                                        }
                                    }
                                    else {
                                        if (!await generator_1.generateCopyCpp(async (copyPath, rootDir) => {
                                            for (const copyFile of ex.files) {
                                                const copyFilePath = path.join(copyPath, copyFile.deployloc);
                                                const copyParent = path.dirname(copyFilePath);
                                                await generator_1.promisifyMkdirp(copyParent);
                                                await utilities_1.promisifyWriteFile(copyFilePath, copyFile.contents);
                                            }
                                            const vendorFiles = await vendorlibs.findForUUIDs(ex.dependencies);
                                            for (const vendorFile of vendorFiles) {
                                                await vendorlibs.installDependency(vendorFile, vendorlibs.getVendorFolder(rootDir), true);
                                            }
                                            return true;
                                        }, path.join(gradleBasePath, ex.gradlebase), folderInto.fsPath, false)) {
                                            vscode.window.showErrorMessage('Cannot create into non empty folder');
                                            return false;
                                        }
                                    }
                                }
                                catch (err) {
                                    logger_1.logger.error('Example generation error: ', err);
                                    return false;
                                }
                                return true;
                            },
                        };
                        core.addExampleProvider(provider);
                    }
                    else {
                        logger_1.logger.log('item not example', ex);
                    }
                }
            }
            else {
                logger_1.logger.log('file not array', fileContents);
            }
        }
    }
    else {
        logger_1.logger.log('no vendor examples found', exampleDir);
    }
}
exports.addVendorExamples = addVendorExamples;
//# sourceMappingURL=vendorexamples.js.map