'use strict';
Object.defineProperty(exports, "__esModule", { value: true });
const fs = require("fs");
function setExecutePermissions(file) {
    if (process.platform === 'win32') {
        return Promise.resolve();
    }
    return new Promise((resolve, reject) => {
        fs.stat(file, (err, stat) => {
            if (err) {
                reject(err);
            }
            else {
                // tslint:disable-next-line:no-bitwise
                let mode = stat.mode & 0xFFFF;
                // tslint:disable-next-line:no-bitwise
                mode |= fs.constants.S_IXUSR | fs.constants.S_IXGRP | fs.constants.S_IXOTH;
                fs.chmod(file, mode, (err2) => {
                    if (err2) {
                        reject(err2);
                    }
                    else {
                        resolve();
                    }
                });
            }
        });
    });
}
exports.setExecutePermissions = setExecutePermissions;
//# sourceMappingURL=permissions.js.map