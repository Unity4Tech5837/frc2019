'use strict';
Object.defineProperty(exports, "__esModule", { value: true });
const electron = require("electron");
const fs = require("fs");
const path = require("path");
const generator_1 = require("./shared/generator");
const utilitiesapi_1 = require("./shared/utilitiesapi");
const vendorlibrariesbase_1 = require("./shared/vendorlibrariesbase");
const utilities_1 = require("./utilities");
const dialog = electron.remote.dialog;
const bWindow = electron.remote.getCurrentWindow();
class VendorLibraries extends vendorlibrariesbase_1.VendorLibrariesBase {
    constructor() {
        super(new utilitiesapi_1.UtilitiesAPI());
    }
    async refreshAvailableDependencies(dir) {
        const deps = await this.getHomeDirDeps();
        const list = document.getElementById('availablelist');
        list.innerHTML = '';
        for (const dep of deps) {
            const innerDiv = document.createElement('li');
            const text = document.createTextNode(`${dep.name} (${dep.version})\t`);
            innerDiv.appendChild(text);
            const button = document.createElement('button');
            button.innerText = 'Install';
            button.dataset.uuid = dep.uuid;
            button.onclick = async () => {
                await this.installOfflineDependency(dir, dep.uuid);
                await this.refreshDependencies(dir);
            };
            innerDiv.appendChild(button);
            list.appendChild(innerDiv);
        }
    }
    async uninstallDependency(dir, uuid) {
        const url = this.getVendorFolder(dir);
        const files = await generator_1.promisifyReadDir(url);
        for (const file of files) {
            const fullPath = path.join(url, file);
            const result = await this.readFile(fullPath);
            if (result !== undefined && uuid === result.uuid) {
                await utilities_1.promisifyDeleteFile(fullPath);
                break;
            }
        }
        await this.refreshDependencies(dir);
    }
    async refreshDependencies(dir) {
        const deps = await this.getDependencies(this.getVendorFolder(dir));
        const list = document.getElementById('installedlist');
        list.innerHTML = '';
        for (const dep of deps) {
            const innerDiv = document.createElement('li');
            const text = document.createTextNode(`${dep.name} (${dep.version})\t`);
            innerDiv.appendChild(text);
            const button = document.createElement('button');
            button.innerText = 'Uninstall';
            button.dataset.uuid = dep.uuid;
            button.onclick = async () => {
                await this.uninstallDependency(dir, dep.uuid);
            };
            innerDiv.appendChild(button);
            list.appendChild(innerDiv);
        }
    }
    async installOfflineDependency(dir, uuid) {
        const deps = await this.getHomeDirDeps();
        let fileDep;
        for (const d of deps) {
            if (d.uuid === uuid) {
                fileDep = d;
                break;
            }
        }
        if (fileDep === undefined) {
            alert('Failed to find dep to install');
            return;
        }
        // Load existing libraries
        const existing = await this.getDependencies(this.getVendorFolder(dir));
        for (const dep of existing) {
            if (dep.uuid === fileDep.uuid) {
                alert('Library already installed');
                return;
            }
        }
        const success = await this.installDependency(fileDep, this.getVendorFolder(dir), true);
        if (success) {
            alert('Successfully installed ' + fileDep.name);
        }
        else {
            alert('Failed to install ' + fileDep.name);
        }
    }
    async installOnlineDependency(dir, url) {
        const file = await this.loadFileFromUrl(url);
        if (file !== undefined) {
            // Load existing libraries
            const existing = await this.getDependencies(this.getVendorFolder(dir));
            for (const dep of existing) {
                if (dep.uuid === file.uuid) {
                    alert('Library already installed');
                    return;
                }
            }
            const success = await this.installDependency(file, this.getVendorFolder(dir), true);
            if (success) {
                alert('Successfully installed ' + file.name);
            }
            else {
                alert('Failed to install ' + file.name);
            }
        }
    }
}
const vendorLibs = new VendorLibraries();
function selectProjectButton() {
    dialog.showOpenDialog(bWindow, {
        buttonLabel: 'Select Project (build.gradle)',
        defaultPath: electron.remote.app.getPath('documents'),
        filters: [
            { name: 'Build Files', extensions: ['gradle'] },
        ],
        message: 'Select your project by selecting build.grade',
        properties: [
            'openFile',
        ],
        title: 'Select your project',
    }, (paths) => {
        if (paths && paths.length === 1) {
            fs.exists(paths[0], async (exists) => {
                if (exists) {
                    const input = document.getElementById('projectFolder');
                    input.value = path.dirname(paths[0]);
                    const div = document.getElementById('validprojectdiv');
                    div.style.display = null;
                    await vendorLibs.refreshDependencies(input.value);
                    await vendorLibs.refreshAvailableDependencies(input.value);
                }
            });
        }
        else {
            // TODO
        }
    });
}
exports.selectProjectButton = selectProjectButton;
async function refreshDependencies() {
    const input = document.getElementById('projectFolder');
    await vendorLibs.refreshDependencies(input.value);
}
exports.refreshDependencies = refreshDependencies;
async function refreshAvailableDependencies() {
    const input = document.getElementById('projectFolder');
    await vendorLibs.refreshAvailableDependencies(input.value);
}
exports.refreshAvailableDependencies = refreshAvailableDependencies;
async function installOnlineLibrary() {
    const urlBox = document.getElementById('onlineurl');
    const url = urlBox.value;
    const input = document.getElementById('projectFolder');
    if (url === undefined || url === '') {
        alert('Empty Online URL. Please enter an online url');
        return;
    }
    try {
        await vendorLibs.installOnlineDependency(input.value, url);
        urlBox.value = '';
        await vendorLibs.refreshDependencies(input.value);
    }
    catch (err) {
        alert('Failed to install dependency because:\n' + err);
    }
}
exports.installOnlineLibrary = installOnlineLibrary;
//# sourceMappingURL=vendordeps.js.map