'use strict';
Object.defineProperty(exports, "__esModule", { value: true });
const electron = require("electron");
const path = require("path");
const examples_1 = require("./shared/examples");
const exampletemplateapi_1 = require("./shared/exampletemplateapi");
const generator_1 = require("./shared/generator");
const templates_1 = require("./shared/templates");
const utilitiesapi_1 = require("./shared/utilitiesapi");
const vendorexamples_1 = require("./shared/vendorexamples");
const vendorlibrariesbase_1 = require("./shared/vendorlibrariesbase");
const utilities_1 = require("./utilities");
const vscode = require("./vscodeshim");
const dialog = electron.remote.dialog;
const bWindow = electron.remote.getCurrentWindow();
let exampleTemplateApi;
function projectSelectButtonClick() {
    document.activeElement.blur();
    dialog.showOpenDialog(bWindow, {
        buttonLabel: 'Select Folder',
        defaultPath: electron.remote.app.getPath('documents'),
        message: 'Select a folder to put the project in',
        properties: [
            'openDirectory',
        ],
        title: 'Select a folder to put the project in',
    }, (paths) => {
        if (paths && paths.length === 1) {
            const input = document.getElementById('projectFolder');
            input.value = paths[0];
        }
        else {
            // TODO
        }
    });
}
exports.projectSelectButtonClick = projectSelectButtonClick;
function selectProjectType() {
    const select = document.getElementById('projectTypeSelect');
    if (select.value !== 'base') {
        // Not a base, enable lang
        const baseSelect = document.getElementById('projectBaseSelect');
        const langSelect = document.getElementById('languageSelect');
        langSelect.disabled = false;
        langSelect.value = 'base';
        baseSelect.disabled = true;
        baseSelect.value = 'base';
    }
    else {
        const baseSelect = document.getElementById('projectBaseSelect');
        const langSelect = document.getElementById('languageSelect');
        langSelect.disabled = true;
        langSelect.value = 'base';
        baseSelect.disabled = true;
        baseSelect.value = 'base';
    }
}
exports.selectProjectType = selectProjectType;
function selectLanguage() {
    const select = document.getElementById('languageSelect');
    if (select.value !== 'base') {
        // Not a base, enable lang
        const baseSelect = document.getElementById('projectBaseSelect');
        const typeSelect = document.getElementById('projectTypeSelect');
        setupBaseSelects(typeSelect.value === 'template', select.value);
        baseSelect.disabled = false;
    }
    else {
        const baseSelect = document.getElementById('projectBaseSelect');
        baseSelect.disabled = true;
        baseSelect.value = 'base';
    }
}
exports.selectLanguage = selectLanguage;
function selectRobotBase() {
    //
}
exports.selectRobotBase = selectRobotBase;
async function generateProjectButtonClick() {
    const typeSelect = document.getElementById('projectTypeSelect');
    const langSelect = document.getElementById('languageSelect');
    const baseSelect = document.getElementById('projectBaseSelect');
    if (typeSelect.value === 'base' || langSelect.type === 'base') {
        alert('project type or language not selected');
        return;
    }
    if (baseSelect.value === 'base') {
        // No base selected, error
        alert('project base not selected');
    }
    else {
        await handleProjectGenerate(typeSelect.value === 'template', langSelect.value, baseSelect.value);
    }
}
exports.generateProjectButtonClick = generateProjectButtonClick;
async function handleProjectGenerate(template, language, base) {
    const projectFolder = document.getElementById('projectFolder').value;
    const projectName = document.getElementById('projectName').value;
    const newFolder = document.getElementById('newFolderCB').checked;
    const teamNumber = document.getElementById('teamNumber').value;
    const desktop = document.getElementById('desktopCB').checked;
    if (!path.isAbsolute(projectFolder)) {
        alert('Can only extract to absolute path');
        return;
    }
    await exampleTemplateApi.createProject(template, language, base, projectFolder, newFolder, projectName, parseInt(teamNumber, 10));
    const toFolder = newFolder ? path.join(projectFolder, projectName) : projectFolder;
    if (desktop) {
        const buildgradle = path.join(toFolder, 'build.gradle');
        await generator_1.setDesktopEnabled(buildgradle, true);
    }
    await utilities_1.promptForProjectOpen(vscode.Uri.file(toFolder));
}
const remote = electron.remote;
// const dialog = remote.dialog;
const app = remote.app;
// const shell = electron.shell;
const basepath = app.getAppPath();
console.log(basepath);
let resourceRoot = path.join(basepath, 'resources');
if (basepath.indexOf('default_app.asar') >= 0) {
    resourceRoot = 'resources';
}
const cppRoot = path.join(resourceRoot, 'cpp');
const gradleRoot = path.join(resourceRoot, 'gradle');
const javaRoot = path.join(resourceRoot, 'java');
let projectRootPath = app.getPath('home');
if (process.platform === 'win32') {
    projectRootPath = app.getPath('documents');
}
const disposables = [];
window.addEventListener('load', async () => {
    exampleTemplateApi = new exampletemplateapi_1.ExampleTemplateAPI();
    const utilitesApi = new utilitiesapi_1.UtilitiesAPI();
    const vendorLibsBase = new vendorlibrariesbase_1.VendorLibrariesBase(utilitesApi);
    const cppExamples = new examples_1.Examples(cppRoot, false, exampleTemplateApi);
    const cppTemplates = new templates_1.Templates(cppRoot, false, exampleTemplateApi);
    const javaExamples = new examples_1.Examples(javaRoot, true, exampleTemplateApi);
    const javaTemplates = new templates_1.Templates(javaRoot, true, exampleTemplateApi);
    await vendorexamples_1.addVendorExamples(resourceRoot, exampleTemplateApi, utilitesApi, vendorLibsBase);
    disposables.push(cppExamples);
    disposables.push(cppTemplates);
    disposables.push(javaExamples);
    disposables.push(javaTemplates);
    console.log(projectRootPath);
    console.log(gradleRoot);
});
document.addEventListener('keydown', (e) => {
    if (e.which === 123) {
        bWindow.webContents.openDevTools();
    }
    else if (e.which === 116) {
        location.reload();
    }
});
function setupBaseSelects(template, language) {
    const select = document.getElementById('projectBaseSelect');
    select.options.length = 0;
    const baseElement = document.createElement('option'); // new HTMLOptionElement();
    baseElement.value = 'base';
    baseElement.innerText = 'Select a project base';
    select.add(baseElement);
    const bases = exampleTemplateApi.getBases(template, language);
    for (const base of bases) {
        const newElement = document.createElement('option');
        newElement.value = base.label;
        newElement.innerText = base.label;
        select.add(newElement);
    }
}
//# sourceMappingURL=projectcreator.js.map