'use strict';
Object.defineProperty(exports, "__esModule", { value: true });
const electron = require("electron");
const wpilib_riolog_1 = require("wpilib-riolog");
const electronimpl_1 = require("../electronimpl");
const sharedscript_1 = require("../shared/sharedscript");
const remote = electron.remote;
function checkResize() {
    sharedscript_1.checkResizeImpl(document.documentElement);
}
exports.checkResize = checkResize;
function scrollImpl() {
    document.documentElement.scrollTop = document.documentElement.scrollHeight;
}
exports.scrollImpl = scrollImpl;
const rioLogWindowView = new electronimpl_1.RioLogWindowView(async (data) => {
    sharedscript_1.handleMessage(data);
});
function sendMessage(message) {
    rioLogWindowView.messageToMain(message);
}
exports.sendMessage = sendMessage;
document.addEventListener('keydown', (e) => {
    if (e.which === 123) {
        remote.getCurrentWindow().webContents.toggleDevTools();
    }
    else if (e.which === 116) {
        location.reload();
    }
});
const rioLog = new wpilib_riolog_1.RioLogWindow(new electronimpl_1.RioLogWebviewProvider(rioLogWindowView), new electronimpl_1.LiveRioConsoleProvider());
window.addEventListener('load', () => {
    rioLog.start(9999);
});
window.addEventListener('unload', () => {
    rioLog.dispose();
});
//# sourceMappingURL=implscript.js.map