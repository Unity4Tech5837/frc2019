'use strict';
Object.defineProperty(exports, "__esModule", { value: true });
const electron = require("electron");
const events_1 = require("events");
const fs = require("fs");
const wpilib_riolog_1 = require("wpilib-riolog");
const dialog = electron.remote.dialog;
class RioLogWindowView extends events_1.EventEmitter {
    constructor(fromMain) {
        super();
        this.fromMain = fromMain;
    }
    messageToMain(data) {
        this.emit('didReceiveMessage', data);
    }
    async postMessage(message) {
        await this.fromMain(message);
        return true;
    }
    async handleSave(saveData) {
        const file = await new Promise((resolve, _) => {
            dialog.showSaveDialog({
                title: 'Select a file to save to',
            }, (f) => {
                resolve(f);
            });
        });
        console.log(file);
        if (file === undefined) {
            return false;
        }
        await new Promise((resolve, _) => {
            fs.writeFile(file, JSON.stringify(saveData, null, 4), 'utf8', () => {
                resolve();
            });
        });
        return true;
    }
    // tslint:disable-next-line:no-empty
    dispose() {
    }
}
exports.RioLogWindowView = RioLogWindowView;
class RioLogWebviewProvider {
    constructor(view) {
        this.view = view;
    }
    createWindowView() {
        return this.view;
    }
}
exports.RioLogWebviewProvider = RioLogWebviewProvider;
class LiveRioConsoleProvider {
    getRioConsole() {
        return new wpilib_riolog_1.RioConsole();
    }
}
exports.LiveRioConsoleProvider = LiveRioConsoleProvider;
//# sourceMappingURL=electronimpl.js.map