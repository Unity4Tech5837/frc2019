'use strict';
Object.defineProperty(exports, "__esModule", { value: true });
const electron = require("electron");
const fs = require("fs");
const path = require("path");
const temp = require("temp");
const dialog = electron.remote.dialog;
function getIsWindows() {
    const nodePlatform = process.platform;
    return nodePlatform === 'win32';
}
exports.getIsWindows = getIsWindows;
function promisifyExists(filename) {
    return new Promise((resolve) => {
        fs.exists(filename, (e) => {
            resolve(e);
        });
    });
}
exports.promisifyExists = promisifyExists;
function promisifyWriteFile(filename, contents) {
    return new Promise((resolve, reject) => {
        fs.writeFile(filename, contents, 'utf8', (err) => {
            if (err) {
                reject(err);
            }
            else {
                resolve();
            }
        });
    });
}
exports.promisifyWriteFile = promisifyWriteFile;
function promisifyReadFile(filename) {
    return new Promise((resolve, reject) => {
        fs.readFile(filename, 'utf8', (err, data) => {
            if (err) {
                reject(err);
            }
            else {
                resolve(data);
            }
        });
    });
}
exports.promisifyReadFile = promisifyReadFile;
function promisifyDeleteFile(file) {
    return new Promise((resolve) => {
        fs.unlink(file, (err) => {
            if (err) {
                resolve(false);
            }
            else {
                resolve(true);
            }
        });
    });
}
exports.promisifyDeleteFile = promisifyDeleteFile;
class ExtensionContext {
    constructor() {
        temp.track();
        this.storagePath = temp.mkdirSync();
    }
}
exports.extensionContext = new ExtensionContext();
async function promptForProjectOpen(toFolder) {
    dialog.showMessageBox({
        buttons: ['Open Folder', 'OK'],
        message: 'Creation of project complete: ' + toFolder.fsPath,
        noLink: true,
    }, (r) => {
        if (r === 0) {
            console.log(toFolder);
            electron.shell.showItemInFolder(path.join(toFolder.fsPath, 'build.gradle'));
        }
        console.log(r);
    });
    return true;
}
exports.promptForProjectOpen = promptForProjectOpen;
//# sourceMappingURL=utilities.js.map