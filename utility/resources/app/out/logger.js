'use strict';
Object.defineProperty(exports, "__esModule", { value: true });
const path = require("path");
const triple_beam_1 = require("triple-beam");
const winston = require("winston");
const myFormat = winston.format.printf((info) => {
    return `${info.timestamp} ${info[triple_beam_1.MESSAGE]}`;
});
const winstonLogger = winston.createLogger({
    exitOnError: false,
    format: winston.format.combine(winston.format.simple(), winston.format.timestamp(), myFormat),
    level: 'verbose',
    transports: [
        new winston.transports.Console(),
    ],
});
function closeLogger() {
    winstonLogger.close();
}
exports.closeLogger = closeLogger;
let mainLogFile = '';
function getMainLogFile() {
    return mainLogFile;
}
exports.getMainLogFile = getMainLogFile;
function setLoggerDirectory(dirname) {
    mainLogFile = path.join(dirname, 'wpilibtoollog.txt');
    winstonLogger.add(new winston.transports.File({
        dirname,
        filename: 'wpilibtoollog.txt',
        level: 'verbose',
        maxFiles: 3,
        maxsize: 1000000,
        tailable: true,
    }));
}
exports.setLoggerDirectory = setLoggerDirectory;
class LoggerImpl {
    // tslint:disable-next-line:no-any
    error(message, ...meta) {
        winstonLogger.log('error', message, meta);
    }
    // tslint:disable-next-line:no-any
    warn(message, ...meta) {
        winstonLogger.log('warn', message, meta);
    }
    // tslint:disable-next-line:no-any
    info(message, ...meta) {
        winstonLogger.log('info', message, meta);
    }
    // tslint:disable-next-line:no-any
    log(message, ...meta) {
        winstonLogger.log('verbose', message, meta);
    }
}
exports.logger = new LoggerImpl();
//# sourceMappingURL=logger.js.map